const fs=eval(require('graceful-fs'));

import 'ol/ol.css';
import './estilos.css';

/*
Openlayer objects
*/
import {Map, View} from 'ol';
import {Tile} from 'ol/layer';
import {OSM} from 'ol/source';
import {fromLonLat} from 'ol/proj';
import {toLonLat} from 'ol/proj';
import {Draw, Modify, Snap} from 'ol/interaction';

/*
OL Geocoder
*/
import 'ol-geocoder/dist/ol-geocoder.min.css';
const Geocoder=require('ol-geocoder');

import {of, fromEvent} from 'rxjs';
import { map, tap } from 'rxjs/operators';

import './estilos.sass';
import bulmaCollapsible from '@creativebulma/bulma-collapsible';

/**/
console.log(bulmaCollapsible);

const lugar_card = document.getElementById("collapsible-lugar");
if (lugar_card){
    const collapsibleLugar= new bulmaCollapsible(lugar_card);

function collapse_lugar(){
    lugar_card.bulmaCollapsible("collapse");
}
    ;
}

const posicion_card = document.getElementById("collapsible-posicion");
if (posicion_card){
    const collapsiblePosicion= new bulmaCollapsible(posicion_card);

    function collapse_posicion(){
        posicion_card.bulmaCollapsible("collapse");
    }
    ;
}


/**/

var ol_map = new Map({
    target: 'map',
    layers:[
        new Tile({source: new OSM()})
    ],
    view: new View({
        center: fromLonLat([-35,-17]),
        zoom:5
    })
});

var geocoder = new Geocoder('nominatim',{
    provider: 'osm',
    key: '__some_key__',
    lang: 'es-LA', //en-US, fr-FR
    placeholder: 'Buscar lugar ...',
    targetType: 'text-input',
    limit: 5,
    keepOpen: true
});

ol_map.addControl(geocoder);

/* the location form */
let lugar_form = document.getElementById('form-lugar').elements;

const select_location = fromEvent(geocoder, 'addresschosen');


let info_stream = select_location.pipe(
    tap(event=>console.log(event)),
    map(event=>{
        let lon_lat = toLonLat(event.coordinate,);
        return {
            x: event.coordinate[0],
            y: event.coordinate[1],
            pais: event.address.details.country,
            ciudad:event.address.details.city,
            nombre: event.address.details.name,
            latitud: lon_lat[0],
            longitud: lon_lat[1],
        };
    }));

info_stream.subscribe({
    next:  data => {
        Object.entries(data).forEach(([key, value])=>{
            if (lugar_form.hasOwnProperty(key)){
                lugar_form[key].value = value;
            }
        });
    },
    error: err => console.log("error",err),
    complete: () => console.log("Selección completa")});


